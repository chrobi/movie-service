import express from 'express'
import axios from 'axios'
const router = express.Router()

import authenticateToken from '../middlewares/authUser.js'
import checkUserLimits from '../middlewares/checkUserLimits.js'
import { omdbApiKey } from '../config/env.js'

import Error400 from '../utils/error/error400.js'

// Models
import Movie from '../models/Movie.js'
import User from '../models/User.js'

router.get('/', authenticateToken, async (req, res, next) => {
	try {
		const { userId } = req.user
		const moviesDoc = await Movie.find({ userId: userId })
		const movies = []

		if (moviesDoc.length !== 0) {
			moviesDoc.forEach((entry) => {
				const movie = {
					Title: entry.title || null,
					Released: entry.released || null,
					Genre: entry.genre || null,
					Director: entry.director || null,
					Timestamps: {
						createdAt: entry.createdAt,
						updatedAt: entry.updatedAt,
					},
				}
				movies.push(movie)
			})
		}

		res.json(movies)
	} catch (err) {
		next(err)
	}
})

router.post('/', authenticateToken, checkUserLimits, async (req, res, next) => {
	try {
		const { title } = req.body
		if (!title) throw new Error400('API Error', 'Missing required fields.')

		const omdbApiEndpoint = `http://www.omdbapi.com/?t=${title}&type=movie&plot=short&r=json&apikey=${omdbApiKey}`
		const { data, status } = await axios.get(omdbApiEndpoint)

		if (status === 200) {
			// Create movie object and then update it if there is external data
			const newMovie = new Movie({
				title: title,
				released: null,
				genre: null,
				director: null,
			})

			if (data.Response !== 'True' || data.Error)
				throw new Error400(
					'API Error',
					`Additional data for the "${title}" movie not found.`,
				)
			else {
				const { userId, role } = req.user
				const movies = await Movie.find({ userId: userId })
				const movieFound = movies.some((movie) => movie.title === data.Title)

				if (movieFound)
					throw new Error400(
						'API Error',
						`The "${data.Title}" movie has already been saved.`,
					)
				else {
					newMovie.userId = userId
					newMovie.title = data.Title
					newMovie.released = data.Released
					newMovie.genre = data.Genre
					newMovie.director = data.Director

					await newMovie.save()

					const userFound = await User.findOne({ id: userId })

					if (userFound) {
						switch (role) {
							case 'basic':
								userFound.limits.requests.count++

								if (userFound.limits.requests.count === 5) {
									const blockExpDate = new Date(
										new Date().getFullYear(),
										new Date().getMonth() + 1,
									)
									userFound.limits.requests.expirationDate = blockExpDate
								}

								await userFound.save()
						}
					} else {
						const newUser = new User({
							id: userId,
							limits: {
								requests: {
									count: 1,
								},
							},
						})
						await newUser.save()
					}

					res
						.status(201)
						.json({ message: `Movie "${data.Title}" saved successfully.` })
				}
			}
		}
	} catch (err) {
		next(err)
	}
})

export default router
