import express from 'express'
import bodyParser from 'body-parser'
import swaggerUi from 'swagger-ui-express'

import movies from './routes/movies.js'

import { returnError } from './middlewares/errorHandler.js'
import Error404 from './utils/error/error404.js'

import dotenv from 'dotenv'
dotenv.config()

const app = express()
app.disable('x-powered-by')

app.use(bodyParser.json())

// Routes
app.use('/movies', movies)
app.use('/docs', swaggerUi.serve, swaggerUi.setup(require('./docs/api.json')))

app.use('*', (req, res, next) => {
	try {
		throw new Error404('API Error', 'Invalid endpoint (check the URL).')
	} catch (err) {
		next(err)
	}
})

// Error handler last in the middlewares chain
app.use(returnError)

export default app
