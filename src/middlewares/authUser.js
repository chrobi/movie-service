import jwt from 'jsonwebtoken'

import { jwtSecret } from '../config/env.js'
import Error401 from '../utils/error/error401.js'
import Error403 from '../utils/error/error403.js'

const authenticateToken = (req, res, next) => {
	try {
		const authHeader = req.headers['authorization']
		const token = authHeader && authHeader.split(' ')[1]

		if (!token) throw new Error401('API Error', 'Missing authentication token.')

		jwt.verify(token, jwtSecret, (err, user) => {
			if (err) throw new Error403('API Error', 'Token authenticate failure.')

			req.user = user

			next()
		})
	} catch (err) {
		next(err)
	}
}

export default authenticateToken
