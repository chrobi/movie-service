import CustomError from '../utils/error/customError.js'

export const logError = (err) => console.error(err)

export const returnError = (err, req, res, next) => {
	res
		.status(err.statusCode || 500)
		.json({ error: { name: err.name, message: err.message } })
}

export const isOperationalError = (error) => {
	if (error instanceof CustomError) {
		return error.isOperational
	}
	return false
}
