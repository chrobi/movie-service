import Error401 from '../utils/error/error401.js'
import Error429 from '../utils/error/error429.js'

import User from '../models/User.js'

const checkUserLimits = async (req, res, next) => {
	try {
		const user = req.user

		if (!user) throw new Error401('API Error', 'User unauthorized.')

		const { userId } = user
		const userFound = await User.findOne({ id: userId })

		if (userFound) {
			switch (user.role) {
				case 'basic':
					if (userFound.limits.requests.count === 5) {
						if (userFound.limits.requests.expirationDate) {
							const currentDate = new Date()

							if (currentDate >= userFound.limits.requests.expirationDate) {
								userFound.limits.requests.expirationDate = undefined
								userFound.limits.requests.count = 0
								await userFound.save()
							} else {
								throw new Error429(
									'API Error',
									'You have reached the requests limit.',
								)
							}
						} else {
							throw new Error429(
								'API Error',
								'You have reached the requests limit.',
							)
						}
					}

					break

				case 'premium':
					break

				default:
					throw new Error429(
						'API Error',
						'You have reached the requests limit.',
					)
			}
		}

		next()
	} catch (err) {
		next(err)
	}
}

export default checkUserLimits
