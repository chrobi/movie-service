export const nodeEnv = process.env.NODE_ENV || 'development'
export const port = process.env.PORT || 2121
export const jwtSecret = process.env.JWT_SECRET || 'holy cow'
export const MongoURI =
	process.env.MONGO_URI || 'mongodb://mongodb/movie-service'
export const authURI =
	process.env.AUTH_SERVICE_URI ||
	`http://auth-service:${process.env.AUTH_SERVICE_PORT}/auth`
export const omdbApiKey = process.env.OMDB_API_KEY || 'a1b2c3'
