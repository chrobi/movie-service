import mongoose from 'mongoose'

const MovieSchema = new mongoose.Schema(
	{
		userId: {
			type: Number,
			required: true,
		},
		title: {
			type: String,
			required: true,
		},
		released: {
			type: Date,
		},
		genre: {
			type: String,
		},
		director: {
			type: String,
		},
	},
	{
		timestamps: true,
	},
)

const Movie = mongoose.model('Movie', MovieSchema)

export default Movie
