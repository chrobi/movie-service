import mongoose from 'mongoose'

const UserSchema = new mongoose.Schema(
	{
		id: {
			type: Number,
			required: true,
			unique: true,
		},
		limits: {
			requests: {
				count: {
					type: Number,
				},
				expirationDate: {
					type: Date,
				},
			},
		},
	},
	{
		timestamps: true,
	},
)

const User = mongoose.model('User', UserSchema)

export default User
