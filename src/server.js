import app from './app.js'
import mongoose from 'mongoose'

import * as env from './config/env.js'
import { logError, isOperationalError } from './middlewares/errorHandler.js'

mongoose
	.connect(env.MongoURI)
	.then(() => console.log('MongoDB connected...'))
	.catch((err) => logError(err))

const PORT = env.port

if (env.nodeEnv === 'production') {
	app.listen(PORT, () =>
		console.log(`Server is live on port ${PORT} (production)`),
	)

	process.on('unhandledRejection', (error) => {
		throw error
	})

	process.on('uncaughtException', (error) => {
		logError(error)
		if (!isOperationalError(error)) process.exit(1)
	})
} else if (env.nodeEnv === 'development') {
	app.listen(PORT, () =>
		console.log(`Server is live on port ${PORT} (development)`),
	)
}
