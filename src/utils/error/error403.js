import httpStatusCodes from './httpStatusCodes.js'
import CustomError from './customError.js'

class Error403 extends CustomError {
	constructor(
		name,
		description,
		statusCode = httpStatusCodes.FORBIDDEN,
		isOperational = true,
	) {
		super(name, description, statusCode, isOperational)
	}
}

export default Error403
