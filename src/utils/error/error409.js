import httpStatusCodes from './httpStatusCodes.js'
import CustomError from './customError.js'

class Error409 extends CustomError {
	constructor(
		name,
		description,
		statusCode = httpStatusCodes.CONFLICT,
		isOperational = true,
	) {
		super(name, description, statusCode, isOperational)
	}
}

export default Error409
