class CustomError extends Error {
	constructor(name, description, statusCode, isOperational) {
		super(description)

		Object.setPrototypeOf(this, new.target.prototype)

		this.name = name
		this.statusCode = statusCode
		this.isOperational = isOperational

		Error.captureStackTrace(this)
	}
}

export default CustomError
