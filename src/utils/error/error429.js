import httpStatusCodes from './httpStatusCodes.js'
import CustomError from './customError.js'

class Error429 extends CustomError {
	constructor(
		name,
		description,
		statusCode = httpStatusCodes.TOO_MANY_REQUESTS,
		isOperational = true,
	) {
		super(name, description, statusCode, isOperational)
	}
}

export default Error429
