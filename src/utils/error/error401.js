import httpStatusCodes from './httpStatusCodes.js'
import CustomError from './customError.js'

class Error401 extends CustomError {
	constructor(
		name,
		description,
		statusCode = httpStatusCodes.UNAUTHORIZED,
		isOperational = true,
	) {
		super(name, description, statusCode, isOperational)
	}
}

export default Error401
