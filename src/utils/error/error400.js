import httpStatusCodes from './httpStatusCodes.js'
import CustomError from './customError.js'

class Error400 extends CustomError {
	constructor(
		name,
		description,
		statusCode = httpStatusCodes.BAD_REQUEST,
		isOperational = true,
	) {
		super(name, description, statusCode, isOperational)
	}
}

export default Error400
