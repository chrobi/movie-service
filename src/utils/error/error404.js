import httpStatusCodes from './httpStatusCodes.js'
import CustomError from './customError.js'

class Error404 extends CustomError {
	constructor(
		name,
		description,
		statusCode = httpStatusCodes.NOT_FOUND,
		isOperational = true,
	) {
		super(name, description, statusCode, isOperational)
	}
}

export default Error404
