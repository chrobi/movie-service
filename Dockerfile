FROM node:16

WORKDIR /movie-service

COPY ./package.json ./package-lock.json ./.babelrc ./run.sh ./
RUN npm install

RUN mkdir ./src
COPY ./src ./src

RUN mkdir ./tests
COPY ./tests ./tests

RUN sed -i 's/\r$//' run.sh && \  
    chmod +x run.sh

EXPOSE 2121

ENTRYPOINT [ "sh", "run.sh" ] 
