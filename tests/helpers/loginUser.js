import axios from 'axios'
import { authURI } from '../../dist/config/env.js'

const user = {
	login: 'basic-thomas',
	password: 'sR-_pcoow-27-6PAwCD8',
}

const getToken = async () => {
	try {
		const { data } = await axios.post(authURI, {
			username: user.login,
			password: user.password,
		})
		return data.token
	} catch (error) {
		console.log(error)
	}
}

export default { getToken }
