import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

let mongodb

const connect = async () => {
	mongodb = await MongoMemoryServer.create()
	mongoose
		.connect(mongodb.getUri())
		.then(() => console.log('MongoDB Memory Server connected...'))
		.catch((err) => logError(err))
}

const close = async () => {
	await mongoose.connection.dropDatabase()
	await mongoose.connection.close()
	await mongodb.stop()
}

const clear = async () => {
	const collections = mongoose.connection.collections
	for (const key in collections) {
		await collections[key].deleteMany({})
	}
}

export default { connect, close, clear }
