import request from 'supertest'

import app from '../../dist/app.js'
import db from '../config/database.js'
import loginUser from '../helpers/loginUser.js'

beforeAll(async () => await db.connect())
afterEach(async () => await db.clear())
afterAll(async () => await db.close())

describe('movies', () => {
	describe('POST /movies', () => {
		it('should respond with 400 http code and error object in JSON body', async () => {
			const token = await loginUser.getToken()

			const response = await request(app)
				.post('/movies')
				.set('Authorization', `Bearer ${token}`)

			expect(response.statusCode).toBe(400)
			expect(response.body).toEqual(
				expect.objectContaining({
					error: expect.objectContaining({
						name: expect.any(String),
						message: expect.any(String),
					}),
				}),
			)
		})

		it('should respond with 400 http code and error object in JSON body', async () => {
			const token = await loginUser.getToken()
			const payload = {
				title: 'title that will not be found in the external service',
			}

			const response = await request(app)
				.post('/movies')
				.set('Authorization', `Bearer ${token}`)
				.send(payload)

			expect(response.statusCode).toBe(400)
			expect(response.body).toEqual(
				expect.objectContaining({
					error: expect.objectContaining({
						name: expect.any(String),
						message: expect.any(String),
					}),
				}),
			)
		})

		it('should respond with 400 http code and error object in JSON body', async () => {
			const token = await loginUser.getToken()
			const payload = {
				title: 'interstellar',
			}

			const res1 = await request(app)
				.post('/movies')
				.set('Authorization', `Bearer ${token}`)
				.send(payload)

			expect(res1.statusCode).toBe(201)

			const res2 = await request(app)
				.post('/movies')
				.set('Authorization', `Bearer ${token}`)
				.send(payload)

			expect(res2.statusCode).toBe(400)
			expect(res2.body).toEqual(
				expect.objectContaining({
					error: expect.objectContaining({
						name: expect.any(String),
						message: expect.any(String),
					}),
				}),
			)
		})

		it('should respond with 401 http code and error object in JSON body', async () => {
			const response = await request(app).post('/movies')

			expect(response.statusCode).toBe(401)
			expect(response.body).toEqual(
				expect.objectContaining({
					error: expect.objectContaining({
						name: expect.any(String),
						message: expect.any(String),
					}),
				}),
			)
		})

		it('should respond with 201 http code and message param in JSON body', async () => {
			const token = await loginUser.getToken()
			const payload = {
				title: 'forrest gump',
			}

			const response = await request(app)
				.post('/movies')
				.set('Authorization', `Bearer ${token}`)
				.send(payload)

			expect(response.statusCode).toBe(201)
			expect(response.body).toEqual(
				expect.objectContaining({
					message: expect.any(String),
				}),
			)
		})
	})

	describe('GET /movies', () => {
		it('should respond with 401 http code and error object in JSON body', async () => {
			const response = await request(app).get('/movies')

			expect(response.statusCode).toBe(401)
			expect(response.body).toEqual(
				expect.objectContaining({
					error: expect.objectContaining({
						name: expect.any(String),
						message: expect.any(String),
					}),
				}),
			)
		})

		it('should respond with 200 http code and an empty array in the response body', async () => {
			const token = await loginUser.getToken()

			const response = await request(app)
				.get('/movies')
				.set('Authorization', `Bearer ${token}`)

			expect(response.statusCode).toBe(200)
			expect(response.body).toEqual(expect.arrayContaining([]))
		})

		it('should respond with 200 http code and an array of objects in the response body', async () => {
			const token = await loginUser.getToken()
			const payload = {
				title: 'interstellar',
			}

			const res1 = await request(app)
				.post('/movies')
				.set('Authorization', `Bearer ${token}`)
				.send(payload)

			expect(res1.statusCode).toBe(201)

			const res2 = await request(app)
				.get('/movies')
				.set('Authorization', `Bearer ${token}`)

			expect(res2.statusCode).toBe(200)
			expect(res2.body).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						Title: expect.any(String),
						Released: expect.any(String),
						Genre: expect.any(String),
						Director: expect.any(String),
						Timestamps: expect.objectContaining({
							createdAt: expect.any(String),
							updatedAt: expect.any(String),
						}),
					}),
				]),
			)
		})
	})
})
