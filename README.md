# movie-service

## TO DOs

- [x] [Project structure]
- [x] [Error handle]
- [x] [API routes]
- [x] [DB connection]
- [x] [Dockerization]
- [x] [External request]
- [x] [User auth]
- [x] [Tests]
- [x] [CI/CD]
- [x] [OpenAPI Doc]

## Description

The API Service written in Express.js that allows user to create movie object based on movie title passed in the request body.

Available operations:

```
GET /movies
```

```
POST /movies
```

Data is stored in the MongoDB.

## Env variables

- NODE_ENV
- PORT
- JWT_SECRET
- MONGO_URI
- AUTH_SERVICE_URI
- OMDB_API_KEY

## Installation

```
JWT_SECRET={secret} OMDB_API_KEY={key} docker-compose up -d
```
